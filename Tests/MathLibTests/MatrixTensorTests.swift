//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/9/23.
//

import Foundation

import XCTest
@testable import MathLib

final class MatrixTensorTests: XCTestCase {
  func testIndex() {
    let m = Matrix(rows: 2, columns: 2, grid: [1,2,3,4])
    let t = Tensor(from: m)
    XCTAssert(t[[0,0]] == 1)
    XCTAssert(t[[0,1]] == 2)
    XCTAssert(t[[1,0]] == 3)
    XCTAssert(t[[1,1]] == 4)
  }
  func testIndexReversion() {
    let m = Matrix(rows: 2, columns: 2, grid: [1,2,3,4])
    let t = Tensor(from: m)
    let rt = t.reversedIndicesOrder()
    XCTAssert(rt[[0,0]] == 1)
    XCTAssert(rt[[0,1]] == 3)
    XCTAssert(rt[[1,0]] == 2)
    XCTAssert(rt[[1,1]] == 4)
  }
}
