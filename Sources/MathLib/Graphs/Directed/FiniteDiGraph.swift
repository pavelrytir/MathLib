//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/10/21.
//

import Foundation

public protocol FiniteDiGraph : DiGraphProtocol {
  associatedtype VertexCollection : Collection
  where VertexCollection.Element == (key: V.Index, value: V)
  associatedtype EdgeCollection : Collection
  where EdgeCollection.Element == (key: E.Index, value: E)
  
  var diVertices: VertexCollection { get }
  var diEdges: EdgeCollection { get }
  var numberOfVertices : Int { get }
  var numberOfEdges : Int { get }
}

extension FiniteDiGraph {
  public var parallelEdges : [[E.Index]] {
    var multiEdges = [[E.Index]]()
    for (vertexId, _) in diVertices {
      let groups = Dictionary(
        grouping: outgoingNeighbors(of: vertexId).map {($0.edge.id, $0.edge.end)}, by: { $0.1 })
      let duplicateGroups = groups.filter {$0.value.count > 1}
      multiEdges.append(contentsOf: duplicateGroups.map {$0.value.map {$0.0}})
    }
    return multiEdges
  }
}
