//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/23.
//

import Foundation

extension DiGraph {
  public func randomlyRemoveVerticesAndKeepConnected(with probabilityToRemove: Double, using generator: inout RandomNumberGenerator) -> DiGraph {
    assert(self.connected)
    var resultGraph = self
    for vertexId in diVertices.keys.sorted() {
      if Double.random(in: 0..<1, using: &generator) <= probabilityToRemove {
        var graphWithRemovedVertex = resultGraph
        graphWithRemovedVertex.removeVertexWith(id: vertexId)
        if graphWithRemovedVertex.connected {
          resultGraph = graphWithRemovedVertex
        }
      }
    }
    return resultGraph
  }
}
