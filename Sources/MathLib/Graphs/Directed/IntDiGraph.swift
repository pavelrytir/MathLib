//
//  IntDiGraph.swift
//  MathLib
//
//  Created by Pavel Rytir on 7/5/20.
//

import Foundation

public typealias IntDiGraph = DiGraph<IntVertex,IntDiEdge>

public struct IntDiEdge : DiEdgeProtocol, Hashable {
  public typealias Index = Int
  public typealias VertexIndex = Int
  public init(id: Index, start from: VertexIndex, end to: VertexIndex) {
    self.start = from
    self.end = to
    self.id = id
  }
  public var id: Index
  public var start: VertexIndex
  public var end: VertexIndex
}



extension IntDiGraph {
  public init(isolatedVertices: [Int]) {
    self = IntDiGraph(isolatedVertices: isolatedVertices.map {IntVertex(id: $0)})
  }
}
