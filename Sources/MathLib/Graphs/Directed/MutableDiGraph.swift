//
//  File.swift
//  
//
//  Created by Pavel Rytir on 12/10/21.
//

import Foundation

public protocol MutableDiGraph : FiniteDiGraph {
  mutating func add(edge: E)
  mutating func add(vertex: V)
  mutating func remove(edge: E)
  mutating func remove(vertex: V)
  mutating func update(edge: E)
  mutating func update(vertex: V)
}

extension MutableDiGraph {
  public mutating func removeAllEdgesBetween(v1Id: V.Index, v2Id: V.Index) {
    let edges = findEdge(from: v1Id, to: v2Id)
    for edge in edges {
      remove(edge: edge)
    }
    let reverseEdges = findEdge(from: v2Id, to: v1Id)
    for edge in reverseEdges {
      remove(edge: edge)
    }
  }
}
