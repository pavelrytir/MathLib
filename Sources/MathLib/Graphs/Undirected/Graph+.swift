//
//  Graph+.swift
//  MathLib
//
//  Created by Pavel Rytir on 7/5/20.
//

import Foundation

extension Graph {
  mutating public func removeParallelEdges(uniquingEdgesWith chooseEdge: ([E]) -> E) {
    var edgesIdToRemove = Set<E.Index>()
    var processedVertices = Set<V.Index>()
    for (vertexId, _) in vertices {
      let vertexNeighbors = neighbors[vertexId]!.filter { $0.vertexId != vertexId && !processedVertices.contains($0.vertexId) } // No loops.
      processedVertices.insert(vertexId)
      let allParallels = Dictionary(grouping: vertexNeighbors, by: { $0.vertexId })
      for (_, parallelNeighbors) in allParallels where parallelNeighbors.count > 1 {
        let parallelEdges = parallelNeighbors.map {edge($0.edgeId)!}
        let edgeToKeep = chooseEdge(parallelEdges)
        for edge in parallelEdges where edge.id != edgeToKeep.id {
          edgesIdToRemove.insert(edge.id)
        }
      }
    }
    for edgeId in edgesIdToRemove {
      remove(edge: edge(edgeId)!)
    }
  }
}

