//
//  BFS.swift
//  MathLib
//
//  Created by Pavel Rytir on 7/23/18.
//


extension FiniteGraph {
  public func breadthFirstSearch(
    from startId: V.Index,
    callback: (V) -> Bool,
    maxDepth : Int? = nil)
  {
    var openSet = Queue<V.Index>()
    var closedSet = Set<V.Index>()
    var hops = [V.Index:Int]()
    
    openSet.enqueue(startId)
    hops[startId] = 0
    
    while let currentVertexId = openSet.dequeue() {
      let currentVertex = vertex(currentVertexId)!
      closedSet.insert(currentVertexId)
      if !callback(currentVertex) {
        return
      }
      if hops[currentVertexId]! < maxDepth ?? Int.max {
        for neigborh in neighbors(of: currentVertexId) where
          !closedSet.contains(neigborh.vertex.id)
        {
          openSet.enqueue(neigborh.vertex.id)
          hops[neigborh.vertex.id] = hops[currentVertexId]! + 1
        }
      }
    }
  }
  
}

