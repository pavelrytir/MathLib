//
//  Components.swift
//  MathLib
//
//  Created by Pavel Rytir on 8/9/18.
//

import Foundation


extension FiniteGraph {
  public var components : [Int: [V.Index]] {
    var comps = [Int:[V.Index]]()
    var unfinished = Set(vertices.map {$0.key})
    var componentNumber = 1
    while !unfinished.isEmpty {
      let start = unfinished.removeFirst()
      var component = [V.Index]()
      breadthFirstSearch(from: start, callback: { vertex in
        component.append(vertex.id)
        unfinished.remove(vertex.id)
        return true
      })
      comps[componentNumber] = component
      componentNumber += 1
    }
    return comps
  }
}

