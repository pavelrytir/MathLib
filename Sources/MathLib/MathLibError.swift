//
//  errors.swift
//  SwiftMath
//
//  Created by Pavel R on 13/06/2018.
//

import Foundation

public struct MathLibError: Error {
  public let message: String
}
