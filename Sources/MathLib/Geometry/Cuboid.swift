//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/23.
//

import Foundation

public struct Cuboid {
  public init(start: Point3D, end: Point3D) {
    self.start = start
    self.end = end
  }
  public let start : Point3D
  public let end : Point3D
}
