//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/23.
//

import Foundation

public struct GraphSegment<T> : DiEdgeProtocol {
  public init(id: Int, start: Int, end: Int, length: T){
    assert(id != -1)
    self.id = id
    self.start = start
    self.end = end
    self.length = length
  }
  public let id: Int
  public let start: Int
  public let end: Int
  public let length: T
}

extension GraphSegment: Codable where T: Codable {}

