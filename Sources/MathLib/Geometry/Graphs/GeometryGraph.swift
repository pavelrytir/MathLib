//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/23.
//

import Foundation

public typealias GeometryGraph<T> = DiGraph<GraphPoint<T>, GraphSegment<T>>

