//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/23.
//

import Foundation

public struct RandomNumberGeneratorDRand48: RandomNumberGenerator {
  public init(seed: Int) {
    // Set the random seed
    srand48(seed)
  }
  
  public func next() -> UInt64 {
    // drand48() returns a Double, transform to UInt64
    UInt64(drand48() * 0x1.0p64) ^ UInt64(drand48() * 0x1.0p16)
  }
}
