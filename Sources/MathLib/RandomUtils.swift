//
//  File.swift
//  
//
//  Created by Pavel Rytir on 1/28/22.
//

import Foundation

public enum RandomUtils {
  public static func sampleIndex(probabilities: [Double]) -> Int {
    let p = Double.random(in: 0..<1)
    var acc = 0.0
    for (i, probability) in probabilities.enumerated() {
      acc += probability
      if p < acc { return i }
    }
    fatalError("Wrong probabilities")
  }
  public static func sampleKey<K: Hashable>(probabilities: [K: Double]) -> K {
    let p = Double.random(in: 0..<1)
    var acc = 0.0
    for (key, probability) in probabilities {
      acc += probability
      if p < acc { return key }
    }
    fatalError("Wrong probabilities")
  }
}
