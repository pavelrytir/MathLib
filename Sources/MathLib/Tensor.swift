//
//  Tensor.swift
//  
//
//  Created by Pavel Rytir on 1/6/23.
//

import Foundation

public struct Tensor<T> {
  public var data: [T]
  public var dimensions: [Int]
  public init(dimensions: [Int], repeating element: T) {
    self.dimensions = dimensions
    self.data = Array(repeating: element, count: dimensions.reduce(1, {$0 * $1}))
  }
  public init(dimensions: [Int], data: [T]) {
    self.dimensions = dimensions
    self.data = data
  }
  public init(from matrix: Matrix) where T == Double {
    self.data = matrix.grid
    self.dimensions = [matrix.rows, matrix.columns]
  }
  public init(from matrices: [Matrix]) where T == Double {
    if matrices.isEmpty {
      self.data = []
      self.dimensions = [0,0,0]
      return
    }
    let size = matrices.first!.grid.count
    let rows = matrices.first!.rows
    let columns = matrices.first!.columns
    for matrix in matrices {
      assert(matrix.grid.count == size)
      assert(matrix.rows == rows)
      assert(matrix.columns == columns)
    }
    
    let newData = matrices.reduce([]) { $0 + $1.grid }
    self.data = newData
    self.dimensions = [matrices.count, rows, columns]
  }
  public var isEmpty: Bool {
    data.isEmpty
  }
  public func isValid(index: [Int]) -> Bool {
    guard index.count == dimensions.count else { return false }
    for i in dimensions.indices {
      assert(index[i] >= 0)
      if index[i] >= dimensions[i] {
        return false
      }
    }
    return true
  }
  private func computePosition(from index: [Int]) -> Int {
    assert(index.count == dimensions.count)
    var position = 0
    var multiplier = 1
    for i in stride(from: dimensions.count - 1, through: 0, by: -1) {
      position += multiplier * index[i]
      multiplier *= dimensions[i]
    }
    return position
  }
  public subscript(index: [Int]) -> T {
    get {
      return self.data[computePosition(from: index)]
    }
    set {
      self.data[computePosition(from: index)] = newValue
    }
  }
  public var indices: TensorIndexSequence {
    TensorIndexSequence(dimensions: dimensions, frozenValues: [:])
  }
  public func indicesWithFrozen(coordinates: [Int: Int]) -> TensorIndexSequence {
    TensorIndexSequence(dimensions: dimensions, frozenValues: coordinates)
  }
  public func reversedIndicesOrder() -> Tensor {
    var reversedTensor = Tensor(dimensions: self.dimensions.reversed(), data: self.data)
    for index in indices {
      let reversedIndex = Array(index.reversed())
      reversedTensor[reversedIndex] = self[index]
    }
    return reversedTensor
  }
  public func transposed(index1: Int, index2: Int) -> Tensor {
    var transposedTensor = self
    for index in indices {
      var transposedIndex = index
      transposedIndex.swapAt(index1, index2)
      transposedTensor[transposedIndex] = self[index]
    }
    return transposedTensor
  }
  public func extendingDimension(to newDimensions: [Int], fillBy element: T) -> Tensor<T> {
    precondition(dimensions.count <= newDimensions.count)
    for dimensionIdx in dimensions.indices {
      precondition(dimensions[dimensionIdx] <= newDimensions[dimensionIdx])
    }
    var newTensor = Tensor(dimensions: newDimensions, repeating: element)
    let indexSuffix = Array<Int>(repeating: 0, count: newDimensions.count - dimensions.count)
    for index in indices {
      if newDimensions.count > dimensions.count {
        if indexSuffix.isEmpty {
          newTensor[index] = self[index]
        } else {
          newTensor[index + indexSuffix] = self[index]
        }
      }
    }
    return newTensor
  }
}

public struct TensorIndexSequence: Sequence {
  let dimensions: [Int]
  let frozenValues: [Int: Int]
  public func makeIterator() -> TensorIndexIterator {
    TensorIndexIterator(dimensions: dimensions, frozenValues: frozenValues)
  }
  public struct TensorIndexIterator: IteratorProtocol {
    let dimensions: [Int]
    var empty: Bool
    let frozenValues: [Int: Int]
    var currentIndex: [Int]
    public init(dimensions: [Int], frozenValues: [Int: Int]) {
      assert((dimensions.min() ?? 0) >= 0)
      self.dimensions = dimensions
      self.frozenValues = frozenValues
      self.currentIndex = Array(repeating: 0, count: dimensions.count)
      self.empty = (dimensions.min() ?? 0) == 0
      for (dim, value) in frozenValues {
        self.currentIndex[dim] = value
      }
    }
    mutating public func next() -> [Int]? {
      if empty { return nil }
      let indexToReturn = currentIndex
      for i in currentIndex.indices where frozenValues[i] == nil {
        if currentIndex[i] == dimensions[i] - 1 {
          currentIndex[i] = 0
        } else {
          currentIndex[i] += 1
          break
        }
      }
      if currentIndex.max()! == 0 {
        empty = true
      }
      return indexToReturn
    }
  }
}



extension Tensor: CustomStringConvertible {
  
  private func renderMatrix(matrixIndex: Int?) -> String {
    let indexPrefix: [Int]
    if let matrixIndex {
      indexPrefix = [matrixIndex]
    } else {
      indexPrefix = []
    }
    var matrixString = ""
    for row in 0..<dimensions[1] {
      var rowString = ""
      for column in 0..<dimensions[2] {
        rowString += " \(self[indexPrefix + [row, column]])"
      }
      matrixString += rowString + "\n"
    }
    return matrixString
  }
  
  public var description: String {
    if dimensions.count > 3 || dimensions.count == 1 {
      return data.map { ", \($0)"}.joined()
    } else if dimensions.count == 2 {
      return renderMatrix(matrixIndex: nil)
    } else {
      var tensorString = ""
      for matrixIndex in 0..<dimensions[0] {
        tensorString += renderMatrix(matrixIndex: matrixIndex) + "\n"
      }
      return tensorString
    }
  }
}
