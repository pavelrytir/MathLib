//
//  StatFunctions.swift
//  MathLib
//
//  Created by Pavel Rytir on 01/11/2018.
//

import Foundation

extension Collection where Element: AdditiveArithmetic {
  public var sum: Element {
    reduce(.zero, +)
  }
  public var cumsum: [Element] {
    var result = [Element]()
    result.reserveCapacity(count)
    var acc = Element.zero
    for e in self {
      acc += e
      result.append(acc)
    }
    return result
  }
  public func subtract(_ other: Self) -> [Element] {
    precondition(self.count == other.count, "Invalid vector sizes.")
    return zip(self, other).map { $0.0 - $0.1 }
  }
}

extension Collection {
  public func someSatisfies(_ predicate: (Element) throws -> Bool) rethrows -> Bool {
    return try !allSatisfy { try !predicate($0) }
  }
}

extension Collection where Element == Double {
  public var avg: Double? {
    guard !isEmpty else {
      return nil
    }
    let sum = self.reduce(0.0, + )
    return sum / Double(count)
  }
  public var variance: Double? {
    guard !isEmpty else {
      return nil
    }
    let sumSquares = reduce(0.0) { $0 + pow($1, 2) }
    return Swift.max(sumSquares / Double(count) - pow(avg!, 2), 0.0)
  }
  public var std: Double? {
    guard let variance = variance else {
      return nil
    }
    return sqrt(variance)
  }
  public func euclideanDistance(to other: Self) -> Double {
    precondition(self.count == other.count, "Invalid vector sizes.")
    return sqrt(zip(self, other).map { pow($0.0 - $0.1, 2) }.reduce(0.0, +))
  }
  public func covariance(to other: Self) -> Double? {
    precondition(self.count == other.count, "Invalid vector sizes.")
    guard self.count >= 1, let selfMean = self.avg, let otherMean = other.avg else {
      return nil
    }
    let sum = zip(self, other).map {($0.0 - selfMean) * ($0.1 - otherMean)}.reduce(0.0, +)
    return sum / Double(count)
  }
  public func correlation(with other: Self) -> Double? {
    precondition(self.count == other.count, "Invalid vector sizes.")
    guard let cov = covariance(to: other), let selfStd = std, let otherStd = other.std else {
      return nil
    }
    return cov / (selfStd * otherStd)
  }
  public var normalized: [Double] {
    let sum = self.sum
    return self.map {$0 / sum}
  }
  public func klDivergence(to other: Self) -> Double {
    zip(self, other).map { $0.0 * log($0.0 / $0.1) }.reduce(0.0, +)
  }
  public func centralMoment(order: Int) -> Double? {
    guard let avg = avg else {
      return nil
    }
    let sum = reduce(0.0) { $0 + pow($1 - avg, Double(order)) }
    return sum / Double(count)
  }
  public var skewness: Double? {
    guard count >= 3, let std = self.std, std != 0.0, let moment3 = centralMoment(order: 3) else {
      return nil
    }
    return moment3 / pow(std, 3)
  }
  
}

extension Collection where Element == Int {
  public var gcd: Int? {
    isEmpty ? nil : self.reduce(0) { MathFunctions.gcd($0, $1) }
  }
}
