//
//  Functions.swift
//  MathLib
//
//  Created by Pavel Rytir on 9/2/18.
//

import Foundation

public enum MathFunctions {
  
  public static func sigmoid(_ x: Double) -> Double {
    return 1/(1 + exp(-x))
  }
  
  public static func gcd(_ a: Int, _ b: Int) -> Int {
    assert(a >= 0)
    assert(b >= 0)
    return b == 0 ? a : gcd(b, a % b)
  }
}
