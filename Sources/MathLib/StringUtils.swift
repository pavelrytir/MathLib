//
//  StringUtils.swift
//  MathLib
//
//  Created by Pavel Rytir on 9/1/20.
//

import Foundation

public enum StringUtils {
  public static func findAllOccurencies(of regex: String, in text: String) -> [Range<String.Index>]
  {
    var ranges: [Range<String.Index>] = []
    var startIndex = text.startIndex
    while let range = text[startIndex...].range(of: regex, options: .regularExpression) {
      ranges.append(range)
      startIndex = range.upperBound
    }
    return ranges
  }
  
}


extension String {
  public func findAllOccurenciesOf(regex: String) -> [Range<String.Index>] {
    return StringUtils.findAllOccurencies(of: regex, in: self)
  }
  
  public func containsNumber() -> Bool {
    return self.range(of: "\\d*", options: .regularExpression) != nil
  }
}
